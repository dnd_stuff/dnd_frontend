# dnd_frontend

A simple frontend test for a poorly named dnd backend

## Before you begin
make sure you have [nodejs](https://nodejs.org/it/) and [yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable) installed on your machine

## Install the dependencies
```bash
yarn
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
yarn run lint
```

### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
